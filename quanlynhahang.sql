-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2021 at 05:30 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlynhahang`
--

-- --------------------------------------------------------

--
-- Table structure for table `cthd`
--

CREATE TABLE `cthd` (
  `STT` int(111) NOT NULL,
  `id_hoadon` int(11) DEFAULT NULL,
  `Ten_Mon` varchar(50) DEFAULT NULL,
  `Gia` double DEFAULT NULL,
  `So_luong` int(11) DEFAULT NULL,
  `Id_Ban` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cthd`
--

INSERT INTO `cthd` (`STT`, `id_hoadon`, `Ten_Mon`, `Gia`, `So_luong`, `Id_Ban`) VALUES
(11, 7, 'Phở', 40000, 1, 0),
(12, 7, 'Bún', 40000, 1, 0),
(13, 7, 'Phở', 40000, 1, 0),
(14, 8, 'Cơm', 30000, 1, 0),
(15, 8, 'Bún', 40000, 1, 0),
(16, 9, 'Bún', 40000, 1, 0),
(17, 9, 'Phở', 40000, 1, 0),
(18, 9, 'Cơm', 30000, 4, 0),
(19, 9, 'Bún', 40000, 1, 0),
(20, 10, 'Bún', 40000, 10, 0),
(21, 10, 'Phở', 40000, 1, 0),
(22, 11, 'Cơm', 30000, 10, 2),
(23, 12, 'Bún', 40000, 10, 0),
(24, 13, 'Cơm', 30000, 1, 2),
(25, 13, 'Bún', 40000, 1, 2),
(26, 13, 'Bún', 40000, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `hoa_don`
--

CREATE TABLE `hoa_don` (
  `id_hoadon` int(111) NOT NULL,
  `Ten_tn` varchar(50) DEFAULT NULL,
  `Ten_kh` varchar(50) DEFAULT NULL,
  `Loai` varchar(50) DEFAULT NULL,
  `Ngay_xuat` datetime DEFAULT NULL,
  `TongTien` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hoa_don`
--

INSERT INTO `hoa_don` (`id_hoadon`, `Ten_tn`, `Ten_kh`, `Loai`, `Ngay_xuat`, `TongTien`) VALUES
(6, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, '----Chọn----', NULL, 400000),
(13, 'Nguyen Dai Phuc', 'A.Huy', 'Tại quán', NULL, 110000);

-- --------------------------------------------------------

--
-- Table structure for table `mon_an`
--

CREATE TABLE `mon_an` (
  `id_mon` int(11) NOT NULL,
  `Ten_mon` varchar(50) NOT NULL,
  `Gia` float NOT NULL DEFAULT 0,
  `Sl` varchar(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mon_an`
--

INSERT INTO `mon_an` (`id_mon`, `Ten_mon`, `Gia`, `Sl`) VALUES
(1, 'Cơm', 30000, '1'),
(2, 'Bún', 40000, '1'),
(3, 'Phở', 40000, '1');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL,
  `Name_role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_table`
--

CREATE TABLE `tbl_table` (
  `id_ban` int(11) NOT NULL,
  `ban` int(11) NOT NULL DEFAULT 0,
  `Trang_thai` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_table`
--

INSERT INTO `tbl_table` (`id_ban`, `ban`, `Trang_thai`) VALUES
(1, 0, 0),
(2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `Ho_ten` varchar(50) DEFAULT NULL,
  `User_name` varchar(50) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Role` int(11) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `Ho_ten`, `User_name`, `Password`, `Role`, `Phone`) VALUES
(1, 'phan trung huy', 'TrungHuy', 'Huy12345', 2, '0366440472'),
(2, 'Nguyễn Đại Phúc', 'PhucXO', 'Huy12345', 2, '0111111'),
(3, 'Nguyen Dai Phuc', 'phuc1', '123a', 2, '0916463622');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cthd`
--
ALTER TABLE `cthd`
  ADD PRIMARY KEY (`STT`) USING BTREE,
  ADD KEY `FK_cthd_hoa_don` (`id_hoadon`);

--
-- Indexes for table `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`id_hoadon`);

--
-- Indexes for table `mon_an`
--
ALTER TABLE `mon_an`
  ADD PRIMARY KEY (`id_mon`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tbl_table`
--
ALTER TABLE `tbl_table`
  ADD PRIMARY KEY (`id_ban`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cthd`
--
ALTER TABLE `cthd`
  MODIFY `STT` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `id_hoadon` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mon_an`
--
ALTER TABLE `mon_an`
  MODIFY `id_mon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_table`
--
ALTER TABLE `tbl_table`
  MODIFY `id_ban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cthd`
--
ALTER TABLE `cthd`
  ADD CONSTRAINT `FK_cthd_hoa_don` FOREIGN KEY (`id_hoadon`) REFERENCES `hoa_don` (`id_hoadon`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
