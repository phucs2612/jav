/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doan;

import doan.*;
import java.sql.Connection;
import java.sql.DriverManager;
import static java.sql.DriverManager.getConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Phucs2612
 */
public class DataConnection {

    public static String DB_URL = "jdbc:mysql://localhost:3306/quanlynhahang";
    public static String USER_NAME = "root";
    public static String PASSWORD = "";
    public static Connection conn;
    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            System.out.println("ket noi thanh cong");

        } catch (Exception e) {
            System.out.println("Ket Noi Khong Thanh Cong");
            conn.close();
        }

    }
}
